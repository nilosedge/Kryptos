package net.nilosplace.Kryptos;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

public class Test2 {

	public enum Direction {
		LEFT,
		RIGHT
	}
	
	static PrintStream out;

	public static void main(String[] args) throws FileNotFoundException {
		String crypt = "?OBKRUOXOGHULBSOLIFBBWFLRVQQPRNGKSSOTWTQSJQSSEKZZWATJKLUDIAWINFBNYPVTTMZFPKWGDKZXTJCDIGKUHUAUEKCAR";

		out = new PrintStream(new File("output.txt"));
		
		
		/*
		System.out.println(crypt.substring(63, 69));
		for(int i = 2; i < 50; i++) {
			char[] temp1 = rotate(crypt.toCharArray(), Direction.RIGHT, i);
			for(int k = 2; k < 50; k++) {
				char[] temp2 = rotate(temp1, Direction.RIGHT, k);
				for(int j = 2; j < 50; j++) {
					char[] temp3 = rotate(temp2, Direction.RIGHT, j);
					if(
							new String(temp1).substring(64, 68).equals("LSML") ||
							new String(temp2).substring(64, 68).equals("LSML") ||
							new String(temp3).substring(64, 68).equals("LSML")
					) {
						System.out.println("Temp1: " + new String(temp1).substring(63, 69));
						System.out.println("Temp2: " + new String(temp2).substring(63, 69));
						System.out.println("Temp3: " + new String(temp3));
						System.out.println("I: " + i + " K: " + k + " J: " + j);
					}
				}
			}
		}
		*/
		
		runnow(crypt.toCharArray(), 3);
		/*
		char[] temp1 = crypt.toCharArray();
		char[] teststring = crypt.toCharArray();
		System.out.println(new String(temp1));
		do {
			
			temp1 = rotate(temp1, Direction.LEFT, 7);
			System.out.println(new String(temp1));
		} while(!(new String(teststring).equals(new String(temp1))));
		//System.out.println(new String(temp1));
		*/
	}
	
	
	public static void runnow(char[] data, int level) {

		level--;
		if(level > 0) {
			for(int i = 2; i < 49; i++) {
				if(level == 2) System.out.println(i);
				char[] temp1 = data;
				char[] teststring = data;
				do {
					temp1 = rotate(temp1, Direction.LEFT, i);
					runnow(temp1, level);
					//out.println(new String(temp1));
					if(new String(temp1).subSequence(64, 68).equals("LSML")) {
						
						out.println(new String(temp1));
					}
				} while(!(new String(teststring).equals(new String(temp1))));
			}
			
			for(int i = 2; i < 49; i++) {
				if(level == 2) System.out.println(i);
				char[] temp1 = data;
				char[] teststring = data;
				do {
					temp1 = rotate(temp1, Direction.RIGHT, i);
					runnow(temp1, level);
					//out.println(new String(temp1));
					if(new String(temp1).subSequence(64, 68).equals("LSML")) {
						out.println(new String(temp1));
					}
				} while(!(new String(teststring).equals(new String(temp1))));
			}
		}
	}
	
	
	
	public static char[] rotate(char[] data, Direction d, int size) {
		char[] ret = new char[data.length];
		
		int rows = (int)Math.ceil(((double)data.length / (double)size));
		
		char[][] matrix = new char[size][rows];
		
		int c = 0;
		for(int k = 0; k < rows; k++) {
			for(int i = 0; i < size; i++) {
				
				if(c < data.length) {
					matrix[i][k] = data[c];
				} else {
					matrix[i][k] = ' ';
				}
				c++;
			}
		}
		
		if(d == Direction.LEFT) {
			c = 0;
			for(int i = (size - 1); i >= 0; i--) {
				for(int k = 0; k < rows; k++) {
					if(matrix[i][k] != ' ') {
						//System.out.println("C: " + matrix[i][k]);
						ret[c] = matrix[i][k];
						c++;
					}
				}
			}
			return ret;
		} else if(d == Direction.RIGHT) {
			c = 0;
			for(int i = 0; i < size; i++) {
				for(int k = (rows - 1); k >= 0; k--) {
					if(matrix[i][k] != ' ') {
						//System.out.println("C: " + matrix[i][k]);
						ret[c] = matrix[i][k];
						c++;
					}
				}
			}
			return ret;
		} else {
			return data;
		}
	}
}