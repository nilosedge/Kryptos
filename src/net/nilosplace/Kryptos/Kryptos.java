package net.nilosplace.Kryptos;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Date;
import java.util.HashMap;

public class Kryptos {

	public enum Direction {
		LEFT,
		RIGHT
	}
	
	private PrintStream out;
	private Playfair playfair2;
	private HashMap<String, String> map;
	
	public static void main(String[] args) throws FileNotFoundException {
		new Kryptos();

	}
	
	public Kryptos() throws FileNotFoundException {
		String crypt = "OBKRUOXOGHULBSOLIFBBWFLRVQQPRNGKSSOTWTQSJQSSEKZZWATJKLUDIAWINFBNYPVTTMZFPKWGDKZXTJCDIGKUHUAUEKCAR";
		playfair2 = new Playfair("KRYPTOS");
		map = new HashMap<String, String>();
		//playfair.dumpGrid();
		out = new PrintStream(new File("output.txt"));
		
		for(char one = 'A'; one <= 'Z'; one++) {
			for(char two = 'A'; two <= 'Z'; two++) {
				map.put(one + "" + two, playfair2.decode(one + "" + two));
			}
		}
		//String decode = playfair.decode("AB");
		
		
		
		
		
		runnow(crypt.toCharArray(), 4);
	}


	
	public void runnow(char[] data, int level) {

		String decode;

		Date s = new Date();
		char[] temp1 = data;
		char[] teststring = data;
		
		for(int i = 2; i < 50; i++) {
			//do {
				temp1 = rotate(temp1, Direction.LEFT, i);
				decode = this.decode(temp1);
				if(decode.subSequence(63, 69).equals("BERLIN")) {
					out.println(decode);
					out.flush();
				}
				if(level > 0) runnow(temp1, level - 1);
			//} while(!(new String(teststring).equals(new String(temp1))));

		}
		
		Date e = new Date();
		//System.out.println("Level: " + level + " half took: " + (e.getTime() - s.getTime()));
		
		temp1 = data;
		teststring = data;
		
		for(int i = 2; i < 50; i++) {
			//do {
				temp1 = rotate(temp1, Direction.RIGHT, i);
				decode = this.decode(temp1);
				if(decode.subSequence(63, 69).equals("BERLIN")) {
					out.println(decode);
					out.flush();
				}
				if(level > 0) runnow(temp1, level - 1);
			//} while(!(new String(teststring).equals(new String(temp1))));
		}
		
		e = new Date();
		if(level > 1) System.out.println("Level: " + level + " took: " + (e.getTime() - s.getTime()));

	}
	
	
	
	public char[] rotate(char[] data, Direction d, int size) {
		char[] ret = new char[data.length];
		
		int rows = (int)Math.ceil(((double)data.length / (double)size));
		
		char[][] matrix = new char[size][rows];
		
		int c = 0;
		for(int k = 0; k < rows; k++) {
			for(int i = 0; i < size; i++) {
				
				if(c < data.length) {
					matrix[i][k] = data[c];
				} else {
					matrix[i][k] = ' ';
				}
				c++;
			}
		}
		
		if(d == Direction.LEFT) {
			c = 0;
			for(int i = (size - 1); i >= 0; i--) {
				for(int k = 0; k < rows; k++) {
					if(matrix[i][k] != ' ') {
						//System.out.println("C: " + matrix[i][k]);
						ret[c] = matrix[i][k];
						c++;
					}
				}
			}
			return ret;
		} else if(d == Direction.RIGHT) {
			c = 0;
			for(int i = 0; i < size; i++) {
				for(int k = (rows - 1); k >= 0; k--) {
					if(matrix[i][k] != ' ') {
						//System.out.println("C: " + matrix[i][k]);
						ret[c] = matrix[i][k];
						c++;
					}
				}
			}
			return ret;
		} else {
			return data;
		}
	}
	
	public String decode(char[] in) {
		String ret = "";
		in = (new String(in) + "X").toCharArray();
		
		for(int i = 0; i < in.length; i+=2) {
			ret += map.get(in[i] + "" + in[i+1]);
		}
		return ret;
	}
	
}
